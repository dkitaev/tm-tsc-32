package ru.tsc.kitaev.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.api.repository.IProjectRepository;
import ru.tsc.kitaev.tm.enumerated.Status;
import ru.tsc.kitaev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.kitaev.tm.model.Project;

import java.util.Optional;
import java.util.function.Predicate;

public final class ProjectRepository extends AbstractOwnerRepository<Project> implements IProjectRepository {

    @NotNull
    public static Predicate<Project> predicateByName(@NotNull final String name) {
        return s -> name.equals(s.getName());
    }

    @NotNull
    @Override
    public Project findByName(@NotNull final String userId, @NotNull final String name) {
        return findAll(userId).stream()
                .filter(predicateByName(name))
                .findFirst()
                .orElseThrow(ProjectNotFoundException::new);
    }

    @NotNull
    @Override
    public Project removeByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final Optional<Project> project = Optional.of(findByName(userId, name));
        project.ifPresent(this::remove);
        return project.orElseThrow(ProjectNotFoundException::new);
    }

    @NotNull
    @Override
    public Project startById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Optional<Project> project = Optional.ofNullable(findById(userId, id));
        project.ifPresent(p -> p.setStatus(Status.IN_PROGRESS));
        return project.orElseThrow(ProjectNotFoundException::new);
    }

    @NotNull
    @Override
    public Project startByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final Optional<Project> project = Optional.of(findByIndex(userId, index));
        project.ifPresent(p -> p.setStatus(Status.IN_PROGRESS));
        return project.orElseThrow(ProjectNotFoundException::new);
    }

    @NotNull
    @Override
    public Project startByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final Optional<Project> project = Optional.of(findByName(userId, name));
        project.ifPresent(p -> p.setStatus(Status.IN_PROGRESS));
        return project.orElseThrow(ProjectNotFoundException::new);
    }

    @NotNull
    @Override
    public Project finishById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Optional<Project> project = Optional.ofNullable(findById(userId, id));
        project.ifPresent(p -> p.setStatus(Status.COMPLETED));
        return project.orElseThrow(ProjectNotFoundException::new);
    }

    @NotNull
    @Override
    public Project finishByIndex(@NotNull final String userId, @NotNull final Integer index) {
        @NotNull final Optional<Project> project = Optional.of(findByIndex(userId, index));
        project.ifPresent(p -> p.setStatus(Status.COMPLETED));
        return project.orElseThrow(ProjectNotFoundException::new);
    }

    @NotNull
    @Override
    public Project finishByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final Optional<Project> project = Optional.of(findByName(userId, name));
        project.ifPresent(p -> p.setStatus(Status.COMPLETED));
        return project.orElseThrow(ProjectNotFoundException::new);
    }

    @Nullable
    @Override
    public Project changeStatusById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final Status status
    ) {
        @Nullable final Optional<Project> project = Optional.ofNullable(findById(userId, id));
        project.ifPresent(p -> p.setStatus(status));
        return project.orElseThrow(ProjectNotFoundException::new);
    }

    @NotNull
    @Override
    public Project changeStatusByIndex(
            @NotNull final String userId,
            @NotNull final Integer index,
            @NotNull final Status status
    ) {
        @NotNull final Optional<Project> project = Optional.of(findByIndex(userId, index));
        project.ifPresent(p -> p.setStatus(status));
        return project.orElseThrow(ProjectNotFoundException::new);
    }

    @NotNull
    @Override
    public Project changeStatusByName(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final Status status
    ) {
        @NotNull final Optional<Project> project = Optional.of(findByName(userId, name));
        project.ifPresent(p -> p.setStatus(status));
        return project.orElseThrow(ProjectNotFoundException::new);
    }

}
