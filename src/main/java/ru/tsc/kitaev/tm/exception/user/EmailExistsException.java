package ru.tsc.kitaev.tm.exception.user;

import ru.tsc.kitaev.tm.exception.AbstractException;

public final class EmailExistsException extends AbstractException {

    public EmailExistsException() {
        super("Error! Email already exists...");
    }

}
